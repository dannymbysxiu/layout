<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width;height=device-height;initial-scale=1;shrink-to-fit=no"> -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="{!! url('css/firstfront/all.css') !!}"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="{!! url('/bs/css/bootstrap.css') !!}">  
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script  type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="{!! url('/bs/js/bootstrap.min.js') !!}" ></script>


  <!-- jquery.fancybox -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.1/jquery.fancybox.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.1/jquery.fancybox.css" media="screen" />
    <title>首頁</title>
<style type="text/css">

  ul.pagination
     {
      margin-left: auto;
      margin-right: auto;
      display: flex;
      align-items: center;
      justify-content: center;
     }
 
     li.page-item a{
     background-color: transparent;
     border:none;
     }
  
    ul.pagination li.active span {

         background-color: #7e212c !important;
             color: white;
             text-decoration: none;
     }
     ul.pagination li .page-link{
       background-color: transparent;
       border:none;
     }
  
  /*pagebox {
    margin-top: 50px;
  }

  ul .pagination {
    margin-left: auto;
    margin-right: auto;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .pagination a {
    color: #555;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color 0.3s;
  }

  .pagination a.active {
    background-color: #7e212c;
    color: white;
    text-decoration: none;
  }

  .pagination a:hover:not(.active) {
    background-color: #EEE;
    text-decoration: none;
    color: #7e212c;
  }*/

</style>
<script type="text/javascript">
  $(document).ready(function() {
 $("a#example1").fancybox();
 $("a.examplebox").fancybox();

 });
</script>
  @yield('head') 
  </head>
<?php

$DBksu_info=DB::table('ksu_info')
->first();
  $ksu_info=array();

$ksu_info['ki_fb']=!empty($DBksu_info) ? $DBksu_info->ki_fb : "";
$ksu_info['ki_url']=!empty($DBksu_info) ? $DBksu_info->ki_url : "";
$ksu_info['ki_qrfile']=!empty($DBksu_info) ? $DBksu_info->ki_qrfile : "";
$ksu_info['ki_youtube']=!empty($DBksu_info) ? $DBksu_info->ki_youtube : "";



?>
  <body style="background-color:white;">
      <div class="sidebar">
        <div class="menu">
          <div id="main">
            <span class="hamburg" onclick="openNav()">&#9776;<p>MENU</p></span>
          </div>
        </div>
        <div class="logo">
          <a href="{{ Action('KsuFrontController@ksuindex') }}"><img src="{!! url('css/firstfront/images/logo.png') !!}"></a>
        </div>
        <div id="mySidenav" class="sidenav">
          <a href="javascript:void(0)" class="closebtn sidenav-a" onclick="closeNav()">&times;</a>
          <a class="sidenav-a" href="{{ Action('KsuFrontController@KsuAbout') }}">崑山資管</a>
          <a class="sidenav-a" href="{{ Action('KsuFrontController@KsuArea') }}">社會責任</a>
          <a class="sidenav-a" href="{{ Action('KsuFrontController@KsuNews') }}">最新消息</a>
          <a class="sidenav-a" href="{{ Action('KsuFrontController@KsuActivity') }}">活動紀錄</a>
          <div class="sidenav-fanslink">
            
          </div>
        </div>
        <div class="fanslink">
          <div class="fanslink-div">
            <a target="_blank" href="{!! $ksu_info['ki_fb'] !!}">
              <i class="fab fa-facebook"></i>
            </a>
          </div>
              
            <div class="fanslink-div">
              <a href="{!! url('/qrcode/small/'.$ksu_info['ki_qrfile']) !!}" id='example1'>
                <i class="fab fa-line"></i>
              </a>
            </div>  
          <div class="fanslink-div">
            <a href="https://www.youtube.com/watch?v={!! $ksu_info['ki_youtube'] !!}" target="_blank">
              <i class="fab fa-youtube"></i>
            </a>
          </div>  
          <div class="fanslink-div">
            <a href="{!! $ksu_info['ki_url'] !!}" target="_blank">
              <i class="fab fa-chrome"></i>
            </a>
          </div>  
        </div>
      </div>
   <div class="content-box">
     <div class="content">
       <div class="page-title">
            @yield('page-title') 
       </div>
       <div class="titleline">
       </div>
      
      @yield('content')
     </div>
    <div class="col-md-12 footer" >
      Copyright © 崑山科技大學資訊管理系 2020. All Rights Reserved   Design by TSY-team
    </div>
   </div>   

  </body>
  <!-- menu選單 -->
  <script>

  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("main").style.display = "none";
  }

  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.getElementById("main").style.display = "block";
  }
  </script>
</html>