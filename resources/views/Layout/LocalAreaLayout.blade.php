<!DOCTYPE html>
<html>
<head>

  <title>@yield('title')</title>
  <!-- Boostrap.CSS -->
  <link rel="stylesheet" href="{!! url('/bs/css/bootstrap.css') !!}">
<meta name="csrf-token" content="{{ csrf_token() }}">
 

  <!-- JS, Popper.js, and jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="{!! url('/bs/js/bootstrap.min.js') !!}" ></script>

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- 選單左邊列css -->
  <link href="{!! url('/css/simple-sidebar.css') !!}" rel="stylesheet">
  <!-- Icon -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

  <!-- Boostrap-JqueryTable -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
  <!-- Ui套件 -->
    <link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <!-- Ckeditor套件 -->
  <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
   <link rel="stylesheet" href="{!! url('/css/form.css') !!}">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/0.7.8/pace.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pace/0.7.8/themes/blue/pace-theme-barber-shop.min.css">

@yield('head')
<style type="text/css">
  .activebox{
    background:#D5c5e9 !important;
  }

</style>
<script type="text/javascript">

  window.onload = function() {
    // document.getElementById("homeSubmenu").classList.add("show");
    var list=document.getElementsByClassName("list-group-item");
    // alert(list);
    var menuindex=sessionStorage.getItem("menukey");
    var menushow=sessionStorage.getItem("menushow");

     list[menuindex].classList.add("addprimary");
  
    if(menushow!=null){
      document.getElementById(menushow).classList.add("show");

    }
  
  };

 
$(function () {
 
    $(".list-group-item").click(function(){
      sessionStorage.setItem("menukey",$('.list-group-item').index(this));

      var href=this.getAttribute("href");
      if(href.indexOf("#")>=0)
      {
        var menuid=href.split("#");
        sessionStorage.setItem("menushow",menuid[1]);
      }
      else
      {
        sessionStorage.setItem("menushow",null);
      }
    });
    

});
function setthis(){

      sessionStorage.setItem("menukey",0);

}
</script>
<?php
$sa_id=Session::get('sa_id');
// dd($sa_id);
if($sa_id==null){
    header('Location:/Longci/public/Reoder');
    // return Redirect::to('user/login')->with('message', 'Login Failed');
    exit();
}
?>
</head>
<body >
  <script type="text/javascript">
    // laravel 傳到前台alert
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
     alert(msg);
     }

  </script>
 <div class="d-flex" id="wrapper">

   <!-- Sidebar -->
   <div class="bg-pink border-right" id="sidebar-wrapper">
     <a href="{{ Action('AboutKsuController@AboutKsuUpdate') }}" onclick="setthis()">
      <div class="sidebar-heading side-header " style="font-size:1.5rem;text-align: center;color: #fff;font-weight: 900;">
        <img src="{!! url('/images/logo.png') !!}">
      </div>
    </a>
     <div class="list-group list-group-flush ">
       <a href="{{ Action('LocalAboutController@LocalAboutUpdate',$sa_id) }}" class="list-group-item list-group-item-action bg-pink">
       <i class="fas fa-edit"></i>
        在地簡介管理
       </a>
       <a href="#Life" data-toggle="collapse"   class="list-group-item list-group-item-action bg-pink ">
        <i class="fas fa-calendar-check"></i>
        生活機能管理
       </a>
       <ul class="collapse list-unstyled" id="Life">
         <li>
           <a href="{{ Action('LifeController@LifeList') }}">
             <i class="fas fa-caret-right"></i>
              生活機能管理
           </a>
         </li>
         <li>
           <a href="{{ Action('LifeTypeController@LifeTypeList') }}">
             <i class="fas fa-caret-right"></i>
             生活機內類別管理
           </a>
         </li> 

       </ul>
       <a href="#GoArea" data-toggle="collapse"   class="list-group-item list-group-item-action bg-pink ">
        <i class="fas fa-calendar-check"></i>
        來去地區管理
       </a>
       <ul class="collapse list-unstyled" id="GoArea">
         <li>
           <a href="{{ Action('GoAttractionsAreaController@GoAttractionsAreaList') }}">
             <i class="fas fa-caret-right"></i>
             景點介紹管理
           </a>
         </li>
         <li>
           <a href="{{ Action('GoShopAreaController@GoShopAreaList') }}">
             <i class="fas fa-caret-right"></i>
             商家介紹管理
           </a>
         </li> 

       </ul>
      <a href="{{ Action('ShopUserController@ShopUserList') }}" class="list-group-item list-group-item-action bg-pink">
       <i class="fas fa-edit"></i>
        地區活動紀錄管理
      </a>
      <a href="{{ Action('SurroundingViewController@SurroundingViewList') }}" class="list-group-item list-group-item-action bg-pink ">
        <i class="fas fa-map-marked-alt"></i>
         環景導覽管理
      </a>

     <a href="#Memery" data-toggle="collapse"   class="list-group-item list-group-item-action bg-pink ">
      <i class="fas fa-map-marked-alt"></i>
      記憶典藏管理
     </a>
     <ul class="collapse list-unstyled" id="Memery">
       <li>
         <a href="{{ Action('MemoryController@MemoryList') }}">
             <i class="fas fa-caret-right"></i>
           記憶典藏管理
         </a>
       </li>
       <li>
         <a href="{{ Action('MemoryController@MemoryTypeList') }}">
           <i class="fas fa-caret-right"></i>
          記憶典藏類別管理
         </a>
       </li> 

     
     </div>
   </div>
   <!-- /#sidebar-wrapper -->

   <!-- Page Content -->
   <div id="page-content-wrapper">

     <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom nvbar-header">
      <div style="width:100%;">
       <button class="btn btn-primary" id="menu-toggle"><i class="fas fa-bars"></i></button>
       <!-- rightmenu -->
        <div class="btn-group float-right">
         <button type="button" class="btn btn-success dropdown-toggle pull-right" data-toggle="dropdown">
          Hi 某某您好
         </button>
         <!-- 重點在這 -->
         <div class="dropdown-menu dropdown-menu-right">
           <a class="dropdown-item" href="{{ Action('LocalFrontController@LocalIndex',$sa_id) }}" target="_blank">瀏覽區域網頁</a>
           <a class="dropdown-item" href="{{ Action('UserLoginController@AdminLoginOut') }}">登出</a>

         </div>
       </div>
       </div>
     </nav>
        <div class="main-content">
          <div class="container-fluid">
            <div class="page-title">
              <h4 >@yield('h4-title')</h4>
            </div>
            @yield('container-fluid')
          </div>
        </div>
   </div>
   <!-- /#page-content-wrapper -->

 </div>
</body>
  <script>

    //左側選單動畫
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    //CK套件
  CKEDITOR.replace( 'editor1' );

  </script>
</html>
