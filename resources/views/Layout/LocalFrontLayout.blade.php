<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="{!! url('css/localfront/all.css') !!}"/>
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{!! url('/bs/css/bootstrap.css') !!}">   

    <!-- Optional JavaScript -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script  type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="{!! url('/bs/js/bootstrap.min.js') !!}" ></script>


  <!-- jquery.fancybox -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.1/jquery.fancybox.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.1/jquery.fancybox.css" media="screen" />
    <title></title>
    @yield('head')
  </head>
  <style type="text/css">

    ul.pagination
       {
        margin-left: auto;
        margin-right: auto;
        display: flex;
        align-items: center;
        justify-content: center;
       }
   
       li.page-item a{
       background-color: transparent;
       border:none;
       }
     ul.pagination  a:active {
        border-color: red !important;
    }
      .nodecoration:hover{
        text-decoration:none;
      }
      ul.pagination li.active span {
  
          border-radius: 16px;
          background-color: #EDA2b6 !important;
          color: white;
          text-decoration: none;
       }
    .active, .jc-highlights-accordion:hover{
      background:none;
    }
    .page-item:last-child .page-link {
      border-radius:16px;
      padding: 8px 16px;


    }
       ul.pagination li .page-link{
         background-color: transparent;
         border:none;
       }
    
       
  </style>
  <script>
  // 往下滑
  // When the user scrolls down 50px from the top of the document, resize the header's font size
  window.onscroll = function() {scrollFunction()};

  function scrollFunction() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      document.getElementById("header").style.backgroundImage = "none";
    } else {
      document.getElementById("header").style.backgroundImage = "  url('/Longci/public/css/localfront/images/top.png') ";
    }

    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
      document.getElementById("header").style.boxShadow = "1px 1px 5px #CCC";
    } else {
      document.getElementById("header").style.boxShadow = "1px 1px 5px #fff";
    }
  }
  </script>


  <!-- 漢堡 -->
  <script>
    window.onload = function() {

    var menuindex=sessionStorage.getItem("frontmenu");

        var NavLink=document.getElementsByClassName('nav-link');
          for(var i=0;i<NavLink.length;i++){
            if(menuindex==i){
              NavLink[i].className="nav-link menuactive";
            }
            else{
               NavLink[i].className="nav-link";
            }
           
          }
    
    };
  function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

  // 一頁式選單
    $(document).on('click', 'a[href^=\\#]', function () { 
            var target = $(this.hash);
               $('html,body').animate({
                   scrollTop: target.offset().top-94+"px"
               }, {duration: 1000,easing: "swing"});
  });
    function menustats(obj){
  
      sessionStorage.setItem("frontmenu",obj);
    }
  </script>
<?php 
$url=explode('/', $_SERVER['REQUEST_URI']);
if(strpos($url[4],'?')==true){
  $ex_uel=explode('?', $url[4]);
  $url[4]=$ex_uel[0];
}


  $DBlocal_about=DB::table('local_about')
  ->where('la_said',$url[4])
  ->first();
    $local_about=array();

  $local_about['la_title']=!empty($DBlocal_about) ? $DBlocal_about->la_title : "";
  $local_about['la_fb']=!empty($DBlocal_about) ? $DBlocal_about->la_fb : "";
  $local_about['la_address']=!empty($DBlocal_about) ? $DBlocal_about->la_address : "";
  $local_about['la_phone']=!empty($DBlocal_about) ? $DBlocal_about->la_phone : "";
  $local_about['la_showtime']=!empty($DBlocal_about) ? $DBlocal_about->la_showtime : "";


  // dd($DBlocal_about);

 
  $society_area=DB::Table('society_area')
  ->where('sa_id',$url[4])
  ->first();
?>
  <body style="background-color:#fff;" id="m0">
    <header>
     <div class="col-md-7" style="height:40px;margin: auto;line-height: 40px;">
      <a href="{{ action('KsuFrontController@ksuindex') }}">
        <p style="text-align: right;color: #7a96a2;letter-spacing: 1px;font-weight: 900;"><i class="fas fa-home">&nbsp;</i>KSU-USR</p>
      </a> 
     </div>
      <nav class="navbar navbar-expand-md " id="header">

      <!-- LOGO -->
        <div class="container">
          <a href="{{ action('LocalFrontController@LocalIndex',$url[4]) }}"  onclick="menustats(null)" class="navbar-brand">
            <img class="logo" src="{!! url('css/localfront/images/leftlogo.png') !!}"  alt="logo">
            <span style="float:right;display:block;font-size:1.5rem;font-weight:900;line-height:77px;">{{$local_about['la_title']}}大小事</span>
          </a>
      <!-- 漢堡選單 -->
          <button class="navbar-toggler" type="button"  data-toggle="collapse" data-target="#navbar-collapse">
            <span class="fas fa-bars"></span>   
          </button>
      <!-- 選單內容 -->
          <div id="navbar-collapse" class="collapse navbar-collapse">

            <ul class="navbar-nav ml-auto">
            @include('Layout.FrontMenu')
           
        
            </ul>
          </div>
        </div>
      </nav>
    </header>
    

    <section>
         @yield('section')
    </section>
    <footer>      
    
      <div class="col-md-12 copy">
        Copyright © {{ $local_about['la_title'] }} 2020. All Rights Reserved   Design by TSY-team
      </div>
      <div class="all-icon-bar">
        
        <a href="{{ $local_about['la_fb'] }}" target="_blank" class="facebook"><i class="fab fa-facebook-f"></i></a> 
        <a href="{{ Action('ShopUserLoginController@ShopUserLogin') }}" target="_blank" class="line"><i class="fas fa-user"></i></a> 
        <a href="{{ Action('KsuFrontController@ksuindex') }}" target="_blank" class="youtube"><i class="fas fa-home"></i></a> 

        <a href="#m0" class="scrolltop"><i class="far fa-arrow-alt-circle-up"></i></a>
      </div> 
    </footer>
  </body>
</html>